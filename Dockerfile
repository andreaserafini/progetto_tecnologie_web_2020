###########
# BUILDER #
###########

FROM python:3.9.6-alpine as builder

WORKDIR /usr/src/app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

#RUN apk update \
#    && apk add postgresql-dev gcc python3-dev musl-dev

RUN apk update \
    && pip install --upgrade pip \
    && apk add --virtual build-deps gcc python3-dev musl-dev \
    && apk add postgresql \
    && apk add postgresql-dev \
    && pip wheel --no-cache-dir --no-deps --wheel-dir /usr/src/app/wheels psycopg2==2.8.6 \
    && apk add jpeg-dev zlib-dev libjpeg \
    && python -m pip wheel --no-cache-dir --no-deps --wheel-dir /usr/src/app/wheels Pillow \
    && apk del build-deps 

COPY ./requirements.txt .
RUN pip wheel --no-cache-dir --no-deps --wheel-dir /usr/src/app/wheels -r requirements.txt

#########
# FINAL #
#########

FROM python:3.9.6-alpine

# USER HOME DIRECTORY:
RUN mkdir -p /home/user

# USER:
RUN addgroup -S user && adduser -S user -G user

# ENVIRONMENT:
ENV HOME=/home/user
ENV APP_HOME=/home/user/progetto_tecnologie_web_2020
RUN mkdir $APP_HOME
RUN mkdir $APP_HOME/staticfiles
RUN mkdir $APP_HOME/media
WORKDIR $APP_HOME

# DEPS:
RUN apk update && apk add libpq jpeg-dev zlib-dev libjpeg
COPY --from=builder /usr/src/app/wheels /wheels
COPY --from=builder /usr/src/app/requirements.txt .
RUN python -m pip install --upgrade pip \
    && pip install --no-cache /wheels/*

# ENTRYPOINT:
COPY ./entrypoint.sh $HOME
RUN chmod +x  $HOME/entrypoint.sh

COPY . $APP_HOME
RUN chown -R user:user $APP_HOME
RUN chown -R user:user $HOME/entrypoint.sh
USER user

ENTRYPOINT ["/home/user/entrypoint.sh"]
