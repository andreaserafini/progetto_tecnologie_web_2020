from django.contrib import admin

# Register your models here.
from songs.models import Genre

admin.site.register(Genre)
