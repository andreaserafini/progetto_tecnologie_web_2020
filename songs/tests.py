from django.urls import reverse

from users.models import User
from django.test import TestCase
import tempfile

# Create your tests here.
from songs.models import Comment, Composer, Like, Song, Genre


# ---- Ajax call test: follow genre/artist ----
#
# To test:
#  - Only available for logged users
#  - If I call the method on myself (try to follow myself) nothing happens
#  - If I call the method with a bad format nothing happens
#  - If I call the method on an artist/genre that doesen't exist nothing happens
#  - If I call the method on an artist/song that i already follow nothing happens
#  - If I call the method on a genre/artist that i don't follow, I start following him
class AjaxCallTest(TestCase):

    def setUp(self):
        self.user_listener = User.objects.create_user(
            username='test',
            first_name='test',
            last_name='test',
            password='psw',
            email='mail@mail.com',
            bio='hi this is a test bio'
        )

        self.user_composer = User.objects.create_user(
            username='test_composer',
            first_name='test_composer',
            last_name='test_composer',
            password='psw',
            email='mail_composer@mail.com',
            bio='hi this is a test bio'
        )

        self.composer = Composer.objects.create(
            art_name='art_name',
            user=self.user_composer
        )

        self.genre = Genre.objects.create(
            name='Rock',
            description='Rock music'
        )

    def test_follow_myself(self):
        self.client.login(
            username="test_composer",
            password="psw"
        )

        response = self.client.get(reverse('songs:follow'),
                                   {'value': 'btn_artist_' + str(self.user_composer.composers.pk)})
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': False, 'artist': False}
        )

        self.assertEqual(self.user_composer.composers.favourite_composers.all().count(), 0)

        self.client.logout()

    def test_bad_format(self):
        self.client.login(
            username="test",
            password="psw"
        )

        response = self.client.get(reverse('songs:follow'),
                                   {'value': 'this_is_a_bad_format' + str(self.user_composer.composers.pk)})

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': False, 'artist': False}
        )

        self.assertEqual(self.user_composer.composers.favourite_composers.all().count(), 0)

        self.client.logout()

    def test_call_on_object_that_doesent_exists(self):
        self.client.login(
            username="test",
            password="psw"
        )

        # Test on composer that doesen't exists
        response = self.client.get(reverse('songs:follow'),
                                   {'value': 'btn_artist' + str(8)})

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': False, 'artist': False}
        )

        # Test on genre that doesen't exists
        response = self.client.get(reverse('songs:follow'),
                                   {'value': 'btn_genre' + str(8)})

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': False, 'artist': False}
        )

        self.client.logout()

    def test_follow_genre_success(self):
        self.assertNotIn(self.genre, self.user_listener.favourite_genres.all())

        self.client.login(
            username="test",
            password="psw"
        )

        response = self.client.get(reverse('songs:follow'),
                                   {'value': 'btn_genre' + str(self.genre.pk)})

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': True, 'artist': False}
        )

        self.assertIn(self.genre, self.user_listener.favourite_genres.all())
        self.user_listener.favourite_genres.clear()

    def test_follow_artist_success(self):
        self.assertNotIn(self.composer, self.user_listener.favourite_composers.all())

        self.client.login(
            username="test",
            password="psw"
        )

        response = self.client.get(reverse('songs:follow'),
                                   {'value': 'btn_artist' + str(self.composer.pk)})

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': True, 'artist': True}
        )

        self.assertIn(self.composer, self.user_listener.favourite_composers.all())
        self.user_listener.favourite_composers.clear()

    def test_follow_already_following(self):
        self.user_listener.favourite_genres.add(self.genre)
        self.assertIn(self.genre, self.user_listener.favourite_genres.all())

        self.client.login(
            username="test",
            password="psw"
        )

        response = self.client.get(reverse('songs:follow'),
                                   {'value': 'btn_genre' + str(self.genre.pk)})

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': True, 'artist': False}
        )

        self.assertIn(self.genre, self.user_listener.favourite_genres.all())


# ---- View test: feed view ----
#
# To test:
#  - Only available for logged users:
#  - Submit form only works if composer
#  - System must recognize if the logged user is a composer or not
#  - The dashboard link should be given only to composer type users
#  - Song belonging to followed artists/genres must be in feed page
class ViewTest(TestCase):
    def setUp(self):
        self.user_listener = User.objects.create_user(
            username='test',
            first_name='test',
            last_name='test',
            password='psw',
            email='mail@mail.com',
            bio='hi this is a test bio'
        )

        self.user_composer = User.objects.create_user(
            username='test_composer',
            first_name='test_composer',
            last_name='test_composer',
            password='psw',
            email='mail_composer@mail.com',
            bio='hi this is a test bio'
        )

        self.null_user_composer = User.objects.create_user(
            username='null_test_composer',
            first_name='test_composer',
            last_name='test_composer',
            password='psw',
            email='mail_composer@mail.com',
            bio='hi this is a test bio'
        )

        self.composer = Composer.objects.create(
            art_name='art_name',
            user=self.user_composer
        )

        self.null_composer = Composer.objects.create(
            art_name='null_art_name',
            user=self.null_user_composer
        )

        self.genre = Genre.objects.create(
            name='Rock',
            description='Rock music'
        )

        self.null_genre = Genre.objects.create(
            name='null',
            description='null music'
        )

        audio = tempfile.NamedTemporaryFile(suffix=".mp3").name
        self.song = Song.objects.create(
            audio_file=audio,
            genre=self.genre,
            title='test_song',
            post='new test song!',
            description='description of the new test song',
            composer=self.composer
        )

        audio = tempfile.NamedTemporaryFile(suffix=".mp3").name
        self.null_song = Song.objects.create(
            audio_file=audio,
            genre=self.null_genre,
            title='null_song',
            post='new test song!',
            description='description of the new test song',
            composer=self.null_composer
        )

    def test_follow_artist_genre_songs_in_feed(self):
        """Test: If i follow an artist or a genre i want to see his/its songs in my feed page"""
        self.client.login(username='test', password='psw')
        self.user_listener.favourite_composers.add(self.composer)
        self.user_listener.favourite_genres.add(self.genre)
        response = self.client.get(reverse('songs:feed'))
        self.user_listener.favourite_composers.remove(self.composer)
        self.user_listener.favourite_genres.remove(self.genre)
        self.assertIn(self.song, response.context['songs'])
        self.assertNotIn(self.null_song, response.context['songs'])
        self.client.logout()

    def test_submit_only_if_composer(self):
        self.client.login(username='test', password='psw')
        count = Song.objects.all().count()
        self.client.post(reverse('songs:feed'), {
            'title': 'title of new song',
            'post': 'post',
            'description': 'description',
            'genre': 'Rock',
            'audio_file': tempfile.NamedTemporaryFile(suffix=".mp3").name
        }
        )
        self.assertEqual(count, Song.objects.all().count(), "The system created a new song even if the method was "
                                                            "called by a listener")
        self.client.logout()

    def test_feed_only_to_authenticated_users(self):
        response = self.client.get(reverse('songs:feed'))
        self.assertEqual(response.url, '/users/login?next=/songs/feed/')

    def test_feed_recognize_composer(self):
        self.client.login(username='test_composer', password='psw')
        response = self.client.get(reverse('songs:feed'))
        self.assertEqual(response.context['is_composer'], True)
        self.client.logout()

    def test_feed_recognize_not_composer(self):
        self.client.login(username='test', password='psw')
        response = self.client.get(reverse('songs:feed'))
        self.assertEqual(response.context['is_composer'], False)
        self.client.logout()

    def test_dashboard_only_to_composer(self):
        self.client.login(username='test', password='psw')
        response = self.client.get(reverse('songs:dashboard'))
        self.assertEqual(response.url, '/songs/become_composer?next=/songs/dashboard')
        self.client.logout()


# ---- Model test: Song  ----
#
# To test:
#   - delete composer -> delete song
#   - delete genre -> delete song
#   - get_comments()
#   - get_likes()
class ModelTestSong(TestCase):
    def setUp(self):
        self.user_listener = User.objects.create_user(
            username='test',
            first_name='test',
            last_name='test',
            password='psw',
            email='mail@mail.com',
            bio='hi this is a test bio'
        )

        self.simple_user = User.objects.create_user(
            username='user',
            first_name='dummy',
            last_name='user',
            password='psw',
            email='mail@mail.com',
            bio='hi this is a test bio'
        )

        self.user_composer = User.objects.create_user(
            username='test_composer',
            first_name='test_composer',
            last_name='test_composer',
            password='psw',
            email='mail_composer@mail.com',
            bio='hi this is a test bio'
        )

        self.composer = Composer.objects.create(
            art_name='art_name',
            user=self.user_composer
        )

        self.genre = Genre.objects.create(
            name='Rock',
            description='Rock music'
        )

        audio = tempfile.NamedTemporaryFile(suffix=".mp3").name
        self.song = Song.objects.create(
            audio_file=audio,
            genre=self.genre,
            title='test_song',
            post='new test song!',
            description='description of the new test song',
            composer=self.composer
        )

    def test_delete_composer_delete_song(self):
        song_count = Song.objects.all().count()
        self.song.composer.delete()
        self.assertEqual(Song.objects.all().count(),
                         song_count - 1,
                         "Deleting composer doesen't delete his song")

    def test_delete_genre_delete_song(self):
        song_count = Song.objects.all().count()
        self.song.composer.delete()
        self.assertEqual(Song.objects.all().count(),
                         song_count - 1,
                         "Deleting genre doesen't delete related song")

    def test_get_comments(self):

        first_comment = Comment.objects.create(
            text="Beautiful song!",
            song=self.song,
            user=self.user_listener
        )

        second_comment = Comment.objects.create(
            text="I don't like this song!",
            song=self.song,
            user=self.simple_user
        )

        self.assertIn(first_comment, self.song.get_comments())
        self.assertIn(second_comment, self.song.get_comments())

    def test_get_likes(self):

        a_like = Like.objects.create(
            user=self.simple_user,
            song=self.song
        )

        another_like = Like.objects.create(
            user=self.user_listener,
            song=self.song
        )

        # When a composer likes his own song...
        like_by_myself = Like.objects.create(
            user=self.composer.user,
            song=self.song
        )

        self.assertIn(a_like.user, self.song.get_likes())
        self.assertIn(another_like.user, self.song.get_likes())
        self.assertIn(like_by_myself.user, self.song.get_likes(), "Like by myself not present...")


# ---- Model test: Composer  ----
#
# To test:
#   - delete user -> delete composer
#   - get_followers()
#   - get_full_name()
#   - get_avatar()
class ModelTestComposer(TestCase):
    def setUp(self):
        self.user_listener = User.objects.create_user(
            username='test',
            first_name='test',
            last_name='test',
            password='psw',
            email='mail@mail.com',
            bio='hi this is a test bio'
        )

        self.simple_user = User.objects.create_user(
            username='user',
            first_name='dummy',
            last_name='user',
            password='psw',
            email='mail@mail.com',
            bio='hi this is a test bio'
        )

        self.user_composer = User.objects.create_user(
            username='test_composer',
            first_name='test_composer',
            last_name='test_composer',
            password='psw',
            email='mail_composer@mail.com',
            bio='hi this is a test bio'
        )

        self.composer = Composer.objects.create(
            art_name='art_name',
            user=self.user_composer
        )

    def test_delete_user_composer_remain(self):
        self.user_composer.delete()
        self.assertEqual(Composer.objects.all().count(), 0)

    def test_get_full_name(self):
        name = self.composer.user.first_name
        surname = self.composer.user.last_name
        self.assertEqual(name + " " + surname, self.composer.get_full_name())

    def test_get_avatar(self):
        self.assertEqual(self.user_composer.get_avatar(), self.composer.get_avatar().file)

    def test_get_followers(self):
        self.composer.favourite_composers.add(self.simple_user)
        self.composer.favourite_composers.add(self.user_listener)

        self.assertIn(self.simple_user, self.composer.get_followers())
        self.assertIn(self.user_listener, self.composer.get_followers())
