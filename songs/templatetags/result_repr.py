from django import template

from songs.models import Song, Composer, Genre

register = template.Library()


@register.filter
def name_filter(obj):
    if type(obj) == Song:
        return obj.title
    elif type(obj) == Composer:
        return obj.art_name
    elif type(obj) == Genre:
        return obj.name
    return 'Error'


@register.filter
def link_filter(obj):
    if type(obj) == Song:
        return 'song/' + str(obj.pk) + '/detail'
    elif type(obj) == Composer:
        return 'composer/' + str(obj.pk) + '/detail'
    elif type(obj) == Genre:
        return 'genre/' + str(obj.pk) + '/detail'
    return '#'
