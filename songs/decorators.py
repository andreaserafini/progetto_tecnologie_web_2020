from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test

from songs.models import Composer


def composer_only(view_func):
    user_is_composer = user_passes_test(
        lambda user: True if Composer.objects.filter(user=user).count() > 0 else False,
        login_url='/songs/become_composer'
    )
    return login_required(user_is_composer(view_func))
