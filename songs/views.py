from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.http import JsonResponse
from django.shortcuts import render
from django.db.models import Q
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, CreateView, UpdateView, DeleteView
from datetime import timedelta

from songs.decorators import composer_only
from songs.models import Song, Genre, Composer, Comment, Like
from users.models import User


@login_required
def feed(request):
    context = dict()

    user = request.user
    is_composer = user.username in [obj.user.username for obj in Composer.objects.all()]

    if request.method == 'POST' and is_composer:
        post_genre = Genre.objects.get(name=request.POST.get("genre"))
        post_composer = Composer.objects.get(user__username=request.user)
        song = Song(
            title=request.POST['title'],
            audio_file=request.FILES['audio_file'],
            description=request.POST['description'],
            post=request.POST.get('post', ''),
            genre=post_genre,
            composer=post_composer
        )
        song.save()

    if is_composer:
        context['art_name'] = user.composers.art_name
        context['genres'] = list(Genre.objects.all())
    favourite_genres = user.favourite_genres.all()
    favourite_composers = user.favourite_composers.all()
    if is_composer:
        songs = Song.objects.filter(
            Q(composer__in=favourite_composers)
            | Q(genre__in=favourite_genres)
            | Q(composer__user__username=user)).order_by('-date')
    else:
        songs = Song.objects.filter(
            Q(composer__in=favourite_composers)
            | Q(genre__in=favourite_genres)).order_by('-date')

    paginator = Paginator(songs, 10)
    page = request.GET.get('page', 1)
    is_paginated = True if paginator.num_pages > 10 else False
    songs = paginator.page(page)

    context['songs'] = songs
    context['is_composer'] = is_composer
    context['avatar'] = user.avatar
    context['full_name'] = user.get_full_name()
    context['bio'] = user.bio
    context['is_paginated'] = is_paginated

    return render(request, 'songs/feed.html', context)


@method_decorator(login_required, name="dispatch")
class ComposerDetailView(DetailView):
    template_name = "songs/composer/detail.html"
    model = Composer


@method_decorator(login_required, name="dispatch")
class ComposerCreateView(CreateView):
    template_name = "songs/composer/create.html"
    model = Composer

    fields = ('art_name',)
    success_url = reverse_lazy('songs:feed')

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.user = self.request.user
        return super(ComposerCreateView, self).form_valid(form)


@method_decorator(login_required, name="dispatch")
class SongDetailView(DetailView):
    template_name = "songs/song/detail.html"
    model = Song


@method_decorator(composer_only, name="dispatch")
class SongChangeView(UpdateView):
    model = Song
    template_name = 'songs/song/change.html'
    fields = ('title', 'post', 'description', 'genre')

    success_url = reverse_lazy('songs:dashboard')


@method_decorator(composer_only, name="dispatch")
class SongDeleteView(DeleteView):
    model = Song
    success_url = reverse_lazy("songs:dashboard")
    template_name = "songs/song/delete.html"


@method_decorator(login_required, name="dispatch")
class GenreDetailView(DetailView):
    template_name = "songs/genre/detail.html"
    model = Genre


@login_required
def insert_comment(request):
    success = False
    user = User.objects.get(username=request.GET.get('username', False))
    song = Song.objects.get(pk=request.GET.get('song', False))
    text = request.GET.get('text', False)

    if user and song and text:
        comment = Comment(
            text=text,
            song=song,
            user=user
        )

        if comment:
            comment.save()
            success = True

    return JsonResponse({'comment_inserted': success})


@login_required
def get_genres(request):
    return JsonResponse({'genres': list(obj.name for obj in Genre.objects.all())})


@login_required
def get_composers(request):
    return JsonResponse({'composers': list(obj.art_name for obj in Composer.objects.all())})


@login_required
def get_songs(request):
    return JsonResponse({'songs': list(obj.title for obj in Song.objects.all())})


@login_required
def following(request):
    return render(request, 'songs/following.html')


@login_required
def unfollow(request):
    success = False
    artist = False
    if request.GET.get('value', False):
        arg = request.GET.get('value')
        if arg.startswith('btn_artist'):
            try:
                artist = Composer.objects.get(pk=int(arg.strip('btn_artist_')))
                user = User.objects.get(username=request.user.username)
                user.favourite_composers.remove(artist)
                success = True
                artist = True
            except:
                success = False
        elif arg.startswith('btn_genre'):
            try:
                genre = Genre.objects.get(pk=int(arg.strip('btn_genre_')))
                user = User.objects.get(username=request.user.username)
                user.favourite_genres.remove(genre)
                success = True
            except:
                success = False

    return JsonResponse({'success': success, 'artist': artist})


@login_required
def follow(request):
    success = False
    artist = False
    if request.GET.get('value', False):
        arg = request.GET.get('value')
        user = request.user
        if arg.startswith('btn_artist'):
            try:
                c = Composer.objects.get(pk=int(arg.strip('btn_artist')))
                if c.user != user:
                    user.favourite_composers.add(c)
                    success = True
                    artist = True
            except:
                success = False
        elif arg.startswith('btn_genre'):
            try:
                genre = Genre.objects.get(pk=int(arg.strip('btn_genre_')))
                user.favourite_genres.add(genre)
                success = True
            except:
                success = False

    return JsonResponse({'success': success, 'artist': artist})


@login_required
def search(request):
    category = request.GET.get('category', False)
    value = request.GET.get('search', False)

    if category and value:
        if category == 'songs':
            res = Song.objects.filter(title__contains=value)
        elif category == 'genres':
            res = Genre.objects.filter(name__contains=value)
        else:
            res = Composer.objects.filter(art_name__contains=value)

        return render(request, 'songs/results.html', {'results': list(res)})


@login_required
def like(request):
    success = False
    user = request.user
    song = Song.objects.get(pk=request.GET.get('pk'))
    if user and song:
        if song.likes.filter(user=user, song=song).count() > 0:
            song.likes.get(user=user, song=song).delete()
        else:
            Like(user=user, song=song).save()

        success = True
    return JsonResponse({'success': success})


@login_required
def rankings(request):
    composers = Composer.objects.all()
    composers_likes = list()
    song_likes = list()

    for composer in composers:
        tot = 0
        songs = Song.objects.filter(composer=composer)
        for song in songs:
            comments = Comment.objects.filter(song=song, date_time__gt=timezone.now() - timedelta(30)) \
                .exclude(user__composers=composer).count()
            tot += comments
        composers_likes.append((composer, tot))

    for song in Song.objects.all():
        song_likes.append((song, song.likes.all().count()))

    song_likes.sort(key=lambda obj: obj[1], reverse=True)
    composers_likes.sort(key=lambda obj: obj[1], reverse=True)

    return render(request, 'songs/ranking.html', {'songs': song_likes[:10], 'composers': composers_likes[:10]})


def daterange(start, end):
    for n in range(int((end - start).days) + 1):
        yield start + timedelta(n)


def get_data(user, start):
    likes = []
    comments = []

    for single_date in daterange(start, timezone.now()):
        likes.append(((single_date.year, single_date.month - 1, single_date.day),
                      Like.objects.filter(song__composer=user.composers, date__lte=single_date).count()))
        comments.append(((single_date.year, single_date.month - 1, single_date.day),
                        Comment.objects.filter(song__composer=user.composers, date_time__lte=single_date)
                         .exclude(user=user).count()))

    return likes, comments


@composer_only
def get_data_to_graph(request, *args, **kwargs):

    start = timezone.now() - timedelta(int(request.GET.get('period')))
    user = request.user
    likes, comments = get_data(user, start)

    return JsonResponse({'likes': likes, 'commenti': comments})


@composer_only
def dashboard(request):
    return render(request, 'songs/dashboard.html')
